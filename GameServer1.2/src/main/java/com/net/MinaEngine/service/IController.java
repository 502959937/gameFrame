/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.net.MinaEngine.service;


import com.dc.gameserver.ServerCore.Service.character.PlayerInstance;
import org.apache.mina.core.buffer.IoBuffer;

import java.io.Serializable;
import java.nio.charset.CharacterCodingException;

/**
 * User: 石头哥哥
 * Date: 13-5-26
 * Time: 上午9:58
 * Connect: 13638363871@163.com
 */
public interface IController extends Serializable {


    /****
     * 使用数组来存储数据包和处理函数的关系，通过数组下标索引来找到相应的操作处理函数的引用，注意，产生的实例引用时单例，所以是线程安全的
     **/
    public static final IController[] GAME_CONTROLLERS = new IController[512];//游戏数据包
    public static final IController[] NEGATIVE_GAME_CONTROLLERS =new  IController[512];//高频游戏数据包

    public void releaseBuf(IoBuffer buffer);

    /**
     * @return
     */
    IoBuffer creatIoBuffer();

    /**
     * @param capacity
     * @return
     */
    IoBuffer creatIoBuffer(int capacity);

    /**
     * 数据包分发    subclass will be implements
     *
     * @param buffer
     * @param player
     */
    void DispatchPack(IoBuffer buffer, PlayerInstance player);

    /**
     * 解析buffer数据为json对象
     *
     * @param buffer
     * @param clazz
     * @return
     */
    Object parseObject(IoBuffer buffer, Class clazz) throws CharacterCodingException;


    /**
     * 解析buffer[]数据为json对象
     *
     * @param buffer
     * @param clazz
     * @return
     */
    Object parseObject(byte[] buffer, Class clazz) throws CharacterCodingException;

    /**
     * read a byte (1 code)
     *
     * @param buffer buffer
     * @return byte
     */
    byte readByte(IoBuffer buffer);

    /**
     * read a short (2 code)
     *
     * @param buffer buffer
     * @return short
     */
    short readShort(IoBuffer buffer);

    /**
     * read a short (4 code)
     *
     * @param buffer buffer
     * @return int
     */
    int readInt(IoBuffer buffer);

    /**
     * @param buffer 待处理数据缓存区
     * @return message
     * 如果默认读取1字节(readBytes())长度的字符串，那么该方法传入buffer将直接返回相应的字符串
     * 编码格式:utf-8
     */
    String readStringUTF8(IoBuffer buffer) throws CharacterCodingException;

    /**
     * @param buffer 待处理数据缓存区
     * @param length 读取指定的数据长度  readBytes() ,readShort() or readLong();
     * @return message
     * 编码格式:utf-8
     */
    String readStringUTF8(IoBuffer buffer, int length) throws CharacterCodingException;


    /**
     * 构建数据包 ，处理一级类型数据包
     * 注意：此方法适应的协议是：
     * paras数据结构-----> length+msg+length+msg ... ...
     * length: byte(1字节),short（2字节）,int（4字节）
     *
     * @param arg1 类型1  注意 ：args1<0
     * @return buffer
     */
    IoBuffer getWriteBufferA(int arg1, Object... paras);

    /**
     * 构建数据包 ，处理二级类型数据包
     *
     * @param arg1  类型1
     * @param arg2  类型2
     * @param paras 待发送的数据
     *              paras数据-----> length+msg+length+msg ... ...
     *              length: byte(1字节),short（2字节）,int（4字节）
     * @return buffer
     */
    IoBuffer getWriteBufferB(int arg1, int arg2, Object... paras);

    /**
     * 是否注册该数据包（只针对数据发:client---->server）
     *
     * @return
     */
    boolean AutoMapper();
}
