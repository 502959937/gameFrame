/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.extComponents.Kit.util;

/**
 * @auther:陈磊 <br/>
 * Date: 13-1-21<br/>
 * Time: 上午12:05<br/>
 * connectMethod:13638363871@163.com<br/>
 */
public class arrayKit {

    public short getShort(byte[]array,int index) {
        return (short) (array[index] << 8 | array[index + 1] & 0xFF);
    }

    public int getUnsignedMedium(byte[]array,int index) {
        return  (array[index]     & 0xff) << 16 |
                (array[index + 1] & 0xff) <<  8 |
                array[index + 2] & 0xff;
    }

    public int getInt(byte[]array,int index) {
        return  (array[index]     & 0xff) << 24 |
                (array[index + 1] & 0xff) << 16 |
                (array[index + 2] & 0xff) <<  8 |
                array[index + 3] & 0xff;
    }

    public long getLong(byte[]array,int index) {
        return  ((long) array[index]     & 0xff) << 56 |
                ((long) array[index + 1] & 0xff) << 48 |
                ((long) array[index + 2] & 0xff) << 40 |
                ((long) array[index + 3] & 0xff) << 32 |
                ((long) array[index + 4] & 0xff) << 24 |
                ((long) array[index + 5] & 0xff) << 16 |
                ((long) array[index + 6] & 0xff) <<  8 |
                (long) array[index + 7] & 0xff;
    }

    public void setShort(byte[]array,int index, int value) {
        array[index]     = (byte) (value >>> 8);
        array[index + 1] = (byte) value;
    }

    public void setMedium(byte[]array,int index, int   value) {
        array[index]     = (byte) (value >>> 16);
        array[index + 1] = (byte) (value >>> 8);
        array[index + 2] = (byte) value;
    }

    public void setInt(byte[]array,int index, int   value) {
        array[index]     = (byte) (value >>> 24);
        array[index + 1] = (byte) (value >>> 16);
        array[index + 2] = (byte) (value >>> 8);
        array[index + 3] = (byte) value;
    }

    public void setLong(byte[]array,int index, long  value) {
        array[index]     = (byte) (value >>> 56);
        array[index + 1] = (byte) (value >>> 48);
        array[index + 2] = (byte) (value >>> 40);
        array[index + 3] = (byte) (value >>> 32);
        array[index + 4] = (byte) (value >>> 24);
        array[index + 5] = (byte) (value >>> 16);
        array[index + 6] = (byte) (value >>> 8);
        array[index + 7] = (byte) value;
    }
}
