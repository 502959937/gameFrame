/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package com.dc.gameserver.exceptions;

/**
 * @author 石头哥哥 </br>
 *         Project : dcserver1.3</br>
 *         Date: 11/24/13  11</br>
 *         Time: 11:53 AM</br>
 *         Connect: 13638363871@163.com</br>
 *         packageName: com.dc.gameserver.serverCore.exceptions</br>
 *         注解：游戏逻辑运行时异常        RuntimeException
 */
public class logicException extends gameException {

    public logicException(String message) {
        super(message);
    }

    public logicException(Throwable cause) {
        super(cause);
    }

    public logicException(String message, Throwable cause) {
        super(message, cause);
    }
}
