/*
 * Copyright (c) 2014. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * http://www.apache.org/licenses/LICENSE-2.0
 */

package RabbitmpTest;


import java.io.IOException;
import java.io.Serializable;

/**
 * @author 石头哥哥 </br>
 *         gameFrame </br>
 *         Date:14-3-12 </br>
 *         Time:下午4:14 </br>
 *         Package:{@link RabbitmpTest}</br>
 *         Comment：    生产者类的任务是向队列里写一条消息
 */
public class Producer extends EndPoint{

    public Producer(String endPointName) throws IOException {
        super(endPointName);
    }

    public void sendMessage(Serializable object) throws IOException {
    }
}
